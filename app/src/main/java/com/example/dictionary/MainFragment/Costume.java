package com.example.dictionary.MainFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.dictionary.Adapter.CostumePagerAdapter;
import com.example.dictionary.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class Costume extends Fragment {
    View v;
    TabLayout tabLayout;
    ViewPager viewPager;
    ArrayList<String> nameOfTheFragment;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.costume_fragment,container,false);
        initView();
        addName();
        assert getFragmentManager() != null;
        viewPager.setAdapter(new CostumePagerAdapter(getFragmentManager(),0,nameOfTheFragment));
        tabLayout.setupWithViewPager(viewPager);
        return v;
    }
    void initView(){
        nameOfTheFragment = new ArrayList<>();
        tabLayout = v.findViewById(R.id.tabLayout);
        viewPager = v.findViewById(R.id.viewPager);
    }
    void addName(){
        nameOfTheFragment.add("Thông thường");
        nameOfTheFragment.add("Chuyên Dụng");
    }
}
