package com.example.dictionary.MainFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.dictionary.Adapter.RopePagerAdapter;
import com.example.dictionary.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class Rope extends Fragment {
    View v;
    TabLayout tabLayout;
    ViewPager viewPager;
    ArrayList<String> nameOfTheFragment;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.rope_fragment,container,false);
        initView();
        addName();
        assert getFragmentManager() != null;
        viewPager.setAdapter(new RopePagerAdapter(getFragmentManager(),0,nameOfTheFragment));
        tabLayout.setupWithViewPager(viewPager);
        return v;
    }
    void initView(){
        nameOfTheFragment = new ArrayList<>();
        tabLayout = v.findViewById(R.id.tabLayoutRope);
        viewPager = v.findViewById(R.id.viewPager);
    }
    void addName(){
        nameOfTheFragment.add("Giới thiệu");
        nameOfTheFragment.add("Thu hồi");
        nameOfTheFragment.add("Nối dây");
        nameOfTheFragment.add("Buộc dây");
        nameOfTheFragment.add("Tạo nút");
        nameOfTheFragment.add("Giữ dây");
    }
}
